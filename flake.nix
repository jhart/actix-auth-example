{
  inputs = {
    naersk.url = "github:nix-community/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        pkgs_common = with pkgs; [ libGL glibc keyutils krb5 libpqxx libxkbcommon openssh openssl pgcli postgresql ];
        naersk-lib = pkgs.callPackage naersk { };
        libPath = with pkgs; lib.makeLibraryPath pkgs_common;
      in
      {
        defaultPackage = naersk-lib.buildPackage ./.;

        packages.hello = pkgs.dockerTools.buildImage {
          name = "hello";
          tag = "0.1.0";
          config = { Cmd = [ "${pkgs.hello}/bin/hello" ]; };
          created = "now";
        };
        
        devShell = with pkgs; mkShell {
          buildInputs = [
            cargo
            cargo-insta
            cargo-nextest
            pre-commit
            rust-analyzer
            rustPackages.clippy
            rustc
            rustfmt
            httpie
            pkgs_common
          ];
          shellHook = ''
            export PKG_CONFIG_PATH="${pkgs.openssl.dev}/lib/pkgconfig";
            export PKG_CONFIG_PATH="${pkgs.postgresql}/lib/pkgconfig:$PKG_CONFIG_PATH";
            export PATH="/run/current-system/sw/bin:/home/knut/.cargo/bin:/home/knut/.nix-profile/bin:$PATH";

            diesel setup --migration-dir ./sci_server/migrations
            diesel migration run --migration-dir ./sci_server/migrations
          '';
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
          LD_LIBRARY_PATH = libPath;
          GIT_EXTERNAL_DIFF = "${difftastic}/bin/difft";
        };
      });
}
