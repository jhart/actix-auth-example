-- Your SQL goes here
CREATE TABLE apikeys (
    id integer PRIMARY KEY,
    user_id uuid NOT NULL,
    api_key VARCHAR NOT NULL
);

CREATE SEQUENCE apikeys_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY apikeys ALTER COLUMN id SET DEFAULT nextval('apikeys_id_seq'::regclass);

ALTER TABLE ONLY apikeys
    ADD CONSTRAINT apikeys_fk_user FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE INITIALLY DEFERRED;

CREATE INDEX apikeys_id_234234 ON apikeys USING btree (user_id);
