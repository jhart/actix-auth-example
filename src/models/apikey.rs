use core::result::Result::Err;
use argon2::{self, Config};
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::helpers::errors::AppError;
use crate::models::user::User;
use crate::schema;

static SALT: &'static str = "random-salt-salt";

#[derive(PartialEq, Deserialize, Serialize, Queryable, Debug, Clone, AsChangeset, Identifiable, Associations)]
#[diesel(table_name = schema::apikeys)]
#[diesel(belongs_to(User))]
pub struct ApiKey {
    pub id: i32,
    pub user_id: Uuid,
    pub api_key: String,
}

#[derive(PartialEq, Deserialize, Serialize, Queryable, Debug, Insertable, Clone, AsChangeset)]
#[diesel(table_name = schema::apikeys)]
pub struct ApiKeyInput {
    pub user_id: Uuid,
    pub api_key: String,
}

impl ApiKey {
    pub fn generate(conn: &mut PgConnection, user_id: &Uuid) -> Result<ApiKey, AppError> {
        use schema::apikeys::dsl::apikeys;

        // Generate API Key
        let config = Config::default();

        let hash = argon2::hash_encoded(&user_id.to_string().as_bytes(), SALT.as_bytes(), &config)
            .unwrap();

        let api_key = ApiKeyInput {
            user_id: user_id.to_owned(),
            api_key: hash,
        };

        // Save it
        let s = match diesel::insert_into(apikeys)
            .values(&api_key)
            .get_result::<ApiKey>(conn) {
                Ok(apikey) => Ok(apikey) ,
                Err(err) => Err(AppError::db_error(err))
            };

        s
    }
}
