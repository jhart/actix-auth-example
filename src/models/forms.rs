use serde::{Deserialize, Serialize};

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct FormRegister {
    pub username: String,
    pub email: String,
    pub password: String,
}

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct FormLogin {
    pub email: String,
    pub password: String,
}

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct FormChangePassword {
    pub password_old: String,
    pub password_new_1: String,
    pub password_new_2: String,
}

