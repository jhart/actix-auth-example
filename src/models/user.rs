use argon2::{self, Config};
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::helpers::errors::AppError;
use crate::models::apikey::ApiKey;
use crate::models::forms::FormRegister;
use crate::schema;

static SALT: &'static str = "random-salt-salt";

#[derive(
    PartialEq,
    Deserialize,
    Serialize,
    Queryable,
    Debug,
    Identifiable,
    Insertable,
    Clone,
    AsChangeset,
)]
#[diesel(table_name = schema::users)]
pub struct User {
    pub id: Uuid,
    pub username: String,
    pub email: String,
    pub password: String,
}

impl User {
    /// Creates a new user in the database from a user form. The provided password is hashed
    pub fn create(conn: &mut PgConnection, form_user: FormRegister) -> Result<User, AppError> {
        use schema::users::dsl::users;

        // Hash password
        let password = form_user.password.as_bytes();
        let config = Config::default();
        let hash = argon2::hash_encoded(password, SALT.as_bytes(), &config).unwrap();

        // Construct user
        let new_user = User {
            id: Uuid::new_v4(),
            username: form_user.username,
            email: form_user.email,
            password: hash,
        };

        // Save API Key and User
        match diesel::insert_into(users)
            .values(&new_user)
            .get_result::<User>(conn)
        {
            Ok(user) => match ApiKey::generate(conn, &new_user.id) {
                Ok(_) => Ok(user),
                Err(err) => Err(err),
            },
            Err(err) => Err(AppError::db_error(err)),
        }
    }

    // Retrieves the user's api key
    pub fn get_api_key(&self, conn: &mut PgConnection) -> Option<String> {
        let api_key = ApiKey::belonging_to(&self).first::<ApiKey>(conn);

        match api_key {
            Ok(api_key) => Some(api_key.api_key),
            Err(_) => None,
        }
    }

    /// Compares a user's password with a provided password
    pub fn check_password(&self, password: &String) -> Result<bool, AppError> {
        let res = argon2::verify_encoded(&self.password, password.as_bytes());
        match res {
            Ok(res) => Ok(res),
            Err(err) => Err(AppError::db_error(err)),
        }
    }

    // Hashes the new password and saves it
    pub fn set_password(&self, conn: &mut PgConnection, password: &String) -> Result<bool, AppError> {
        use schema::users::dsl::{password as u_pwd, users};

        let config = Config::default();
        let hash = argon2::hash_encoded(password.as_bytes(), SALT.as_bytes(), &config).unwrap();

        match diesel::update(users).set(u_pwd.eq(hash)).execute(conn) {
            Ok(_) => Ok(true),
            Err(err) => Err(AppError::db_error(err)),
        }
    }

    /// Fetches all users from the database
    pub fn get_users(conn: &mut PgConnection) -> Result<Vec<User>, AppError> {
        use schema::users::dsl::*;

        users
            .load::<User>(conn)
            .map_err(|err| AppError::db_error(err))
    }

    /// Fetches a user by email
    pub fn get_by_email(conn: &mut PgConnection, email: &String) -> Result<User, AppError> {
        use schema::users::dsl::{email as u_email, users};

        users
            .filter(u_email.eq(email))
            .first::<User>(conn)
            .map_err(|err| AppError::db_error(err))
    }

    /// Fetches a user by uuid
    pub fn get_by_uuid(conn: &mut PgConnection, uuid: &Uuid) -> Result<User, AppError> {
        use schema::users::dsl::{id as u_id, users};

        users
            .filter(u_id.eq(uuid))
            .first::<User>(conn)
            .map_err(|err| AppError::db_error(err))
    }
}
