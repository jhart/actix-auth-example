// @generated automatically by Diesel CLI.

diesel::table! {
    apikeys (id) {
        id -> Int4,
        user_id -> Uuid,
        api_key -> Varchar,
    }
}

diesel::table! {
    users (id) {
        id -> Uuid,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
    }
}

diesel::joinable!(apikeys -> users (user_id));

diesel::allow_tables_to_appear_in_same_query!(
    apikeys,
    users,
);
