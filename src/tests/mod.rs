use crate::db::TestContext;
use crate::db::get_test_pool;
use crate::models::app_state::AppState;
use crate::models::forms::{FormRegister, FormLogin};
use crate::routes::register::RegisterResponse;
use crate::routes;
use crate::helpers::secrets::Secret;
use crate::helpers::config::Config;
use actix_session::{storage::RedisActorSessionStore, SessionMiddleware};
use actix_web::{test, web, App};
use actix_web::http::header;
use actix_service::Service;

pub const EMAIL: &'static str = "test@test.de";
pub const PASSWORD: &'static str = "passw$$ord";
pub const USERNAME: &'static str = "myusername";

/// Test helper. Registers a user, logs in and returns the session cookie
pub async fn test_setup(ctx: &TestContext) -> (String, impl Service<actix_http::Request, Response = actix_web::dev::ServiceResponse, Error = actix_web::Error>) {

    let app_state = AppState {
        pool: get_test_pool(ctx.db_name.clone()),
        log: Config::configure_log(),
    };

    let settings = Config::init();
    let secret_key = Secret::generate_key();

    let app = test::init_service(
        App::new()
            .app_data(web::Data::new(app_state))
            .wrap(
                SessionMiddleware::builder(
                    RedisActorSessionStore::new(settings.redis_uri.clone()),
                    secret_key.clone(),
                )
                .cookie_name("session_id".to_string())
                .build(),
            )
            .service(routes::register::register)
            .service(routes::logout::logout)
            .service(routes::change_password::change_password)
            .service(routes::login::login),
    )
    .await;

    // Register User
    let form_logout = FormRegister {
        username: USERNAME.to_string(),
        email: EMAIL.to_string(),
        password: PASSWORD.to_string(),
    };

    let req = test::TestRequest::post()
        .uri("/register")
        .set_json(form_logout)
        .to_request();

    let _: RegisterResponse = test::call_and_read_body_json(&app, req).await;

    // Login
    let form_login = FormLogin {
        email: EMAIL.to_string(),
        password: PASSWORD.to_string(),
    };

    let req = test::TestRequest::post()
        .uri("/login")
        .set_json(form_login)
        .to_request();

    let resp_login = test::call_service(&app, req).await;

    // Get session cookie
    let mut cookie = "".to_string();
    match resp_login.headers().get(header::SET_COOKIE) {
        Some(cookie2) => {
            let c = match cookie2.to_str() {
                Ok(s) => s.to_string(),
                Err(err) => panic!("To string error: {}", err.to_string())
            };
            let c = match c.split_once(";") {
                Some(s) => s,
                None => panic!("Some error")
            };
            cookie = c.clone().0.to_string();
        }
        None => {
            cookie = cookie;
        }
    };
    (cookie, app)
}
