use crate::db::get_conn;
use crate::helpers::errors::{log_error, log_error_type, AppError, AppErrorType};
use crate::helpers::validators::validate_password;
use crate::helpers::session_state::TypedSession;
use crate::models::app_state::AppState;
use crate::models::forms::FormChangePassword;
use crate::models::user::User;

use actix_web::{post, web, Responder, Result};
use serde::{Deserialize, Serialize};
use slog::o;

#[derive(Serialize, Deserialize, Default)]
pub struct ChangePasswordResponse {
    success: bool,
    error: Option<String>,
}

#[post("/change_password")]
async fn change_password(
    form: web::Json<FormChangePassword>,
    state: web::Data<AppState>,
    session: TypedSession,
) -> Result<impl Responder, AppError> {
    let sublog = state.log.new(o!("handler" => "change_password"));

    let conn = &mut get_conn(state.pool.clone(), sublog.clone()).await?;

    // Get user_id from session
    let user_id = match session.get_user_id() {
        Ok(user_id) => user_id,
        Err(err) => return Err(log_error(sublog)(err)),
    };

    // Get user
    let user = match User::get_by_uuid(conn, &user_id) {
        Ok(user) => user,
        Err(err) => return Err(log_error(sublog)(err)),
    };

    // Check old password
    match user.check_password(&form.password_old) {
        Ok(check) => {
            if !check {
                return Err(log_error_type(sublog)(AppErrorType::PasswordWrong));
            }
        }
        Err(err) => return Err(err),
    };

    // Check if new passwords match
    if form.password_new_1 != form.password_new_2 {
        return Err(log_error_type(sublog)(AppErrorType::PasswordsDontMatch));
    }

    // Test password
    // TODO: use something can can check lookaheads and allow passwords that contain at least one special character
    if !validate_password(&form.password_new_1) {
        return Err(log_error_type(sublog)(AppErrorType::PasswordInvalid));
    }

    match user.set_password(conn, &form.password_new_1) {
        Ok(_) => Ok(web::Json(ChangePasswordResponse {
            success: true,
            error: None,
        })),
        Err(e) => Err(log_error(sublog)(e)),
    }
}

// --------------------------
// TESTS --------------------
// --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use crate::db::TestContext;
    use crate::helpers::errors::AppErrorResponse;
    use crate::tests::test_setup;
    use crate::tests::PASSWORD;

    use crate::helpers::messages::*;
    use actix_web::http::header;
    use actix_web::test;
    use serde::de::DeserializeOwned;

    async fn test_change_password_setup<T: DeserializeOwned>(form: FormChangePassword) -> T {
        let ctx = TestContext::new();

        let (cookie, app) = test_setup(&ctx).await;

        // Change password
        let req = test::TestRequest::post()
            .uri("/change_password")
            .append_header((header::COOKIE, cookie.clone()))
            .set_json(form)
            .to_request();

        let resp: T = test::call_and_read_body_json(&app, req).await;
        resp
    }

    #[actix_web::test]
    /// Returns an error when not logged in
    async fn test_change_pwd_not_logged_in() {
        let ctx = TestContext::new();

        let (_, app) = test_setup(&ctx).await;

        let form = FormChangePassword {
            password_old: PASSWORD.to_string(),
            password_new_1: "AAAAAA".to_string(),
            password_new_2: "AAAAAA".to_string(),
        };

        // Change password
        let req = test::TestRequest::post()
            .uri("/change_password")
            .set_json(form)
            .to_request();

        let resp: AppErrorResponse = test::call_and_read_body_json(&app, req).await;

        assert_eq!(resp.error, MSG_NOT_LOGGED_IN.to_string());
    }

    #[actix_web::test]
    /// Returns an error when password is wrong
    async fn test_change_pwd_wrong_pwd() {
        let form = FormChangePassword {
            password_old: "wrong".to_string(),
            password_new_1: "AAAAAA".to_string(),
            password_new_2: "AAAAAA".to_string(),
        };

        let resp: AppErrorResponse = test_change_password_setup(form).await;

        assert_eq!(resp.error, MSG_PASSWORD_WRONG.to_string());
    }

    #[actix_web::test]
    /// Returns an error when new passwords don't match
    async fn test_change_pwd_pwd_dont_match() {
        let form = FormChangePassword {
            password_old: PASSWORD.to_string(),
            password_new_1: "AAAAAA".to_string(),
            password_new_2: "AAsdfsdfAAAA".to_string(),
        };

        let resp: AppErrorResponse = test_change_password_setup(form).await;

        assert_eq!(resp.error, MSG_PASSWORDS_DONT_MATCH.to_string());
    }

    #[actix_web::test]
    /// Returns an error when the new password is invalid
    async fn test_change_pwd_pwd_invalid() {
        let form = FormChangePassword {
            password_old: PASSWORD.to_string(),
            password_new_1: "aa".to_string(),
            password_new_2: "aa".to_string(),
        };

        let resp: AppErrorResponse = test_change_password_setup(form).await;

        assert_eq!(resp.error, MSG_PASSWORD_INVALID.to_string());
    }

    #[actix_web::test]
    /// Successfully update password
    async fn test_change_pwd_success() {
        let form = FormChangePassword {
            password_old: PASSWORD.to_string(),
            password_new_1: "AAsdf$$sdfAAAA".to_string(),
            password_new_2: "AAsdf$$sdfAAAA".to_string(),
        };

        let resp: ChangePasswordResponse = test_change_password_setup(form).await;

        assert_eq!(resp.error, None);
        assert_eq!(resp.success, true);
    }
}
