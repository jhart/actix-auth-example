use crate::db::get_conn;
use crate::helpers::errors::{log_error, log_error_type, AppError, AppErrorType};
use crate::helpers::session_state::TypedSession;
use crate::models::app_state::AppState;
use crate::models::forms::FormLogin;
use crate::models::user::User;

use actix_web::{post, web, Responder, Result};
use serde::{Deserialize, Serialize};
use slog::o;
use uuid::Uuid;

#[derive(Serialize, Clone, Default, Deserialize, Debug, PartialEq)]
pub struct UserStub {
    pub id: Uuid,
    pub username: String,
    pub email: String,
}

#[derive(Serialize, Deserialize)]
pub struct LoginResponse {
    pub user: Option<UserStub>,
    pub error: Option<String>,
    pub api_key: Option<String>,
}

impl LoginResponse {
    pub fn new_error(msg: &str) -> Self {
        Self {
            user: None,
            error: Some(msg.to_string()),
            api_key: None,
        }
    }
}

#[post("/login")]
async fn login(
    form_login: web::Json<FormLogin>,
    state: web::Data<AppState>,
    session: TypedSession,
) -> Result<impl Responder, AppError> {
    let sublog = state.log.new(o!("handler" => "login"));

    let conn = &mut get_conn(state.pool.clone(), sublog.clone()).await?;

    // Get user
    let user = match User::get_by_email(conn, &form_login.email) {
        Ok(user) => user,
        Err(_) => return Err(log_error_type(sublog)(AppErrorType::LoginWrongCredentials)),
    };

    // Check password
    match user.check_password(&form_login.password) {
        Ok(res) => match res {
            // Successful login
            true => true,
            false => return Err(log_error_type(sublog)(AppErrorType::LoginWrongCredentials)),
        },
        Err(err) => {
            return Err(log_error(sublog)(AppError {
                message: None,
                cause: Some(err.to_string()),
                error_type: AppErrorType::LoginWrongCredentials,
            }))
        }
    };

    // Insert user_id into session
    session.renew();
    match session.insert_user_id(user.id) {
        Ok(_) => Ok(web::Json(LoginResponse {
            user: Some(UserStub {
                id: user.clone().id,
                username: user.clone().username,
                email: user.clone().email,
            }),
            error: None,
            api_key: user.get_api_key(conn),
        })),
        Err(e) => Err(AppError {
            message: None,
            cause: Some(e.to_string()),
            error_type: AppErrorType::SessionInsertError,
        }),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::config::Config;
    use crate::helpers::messages::*;
    use crate::models::forms::FormRegister;
    use crate::routes::register::{register, RegisterResponse};
    use actix_web::{test, App};

    const EMAIL: &'static str = "test@test.de";
    const PASSWORD: &'static str = "passw$ord";

    async fn test_login(form: FormLogin) -> LoginResponse {
        use crate::db::get_test_pool;
        use crate::db::TestContext;

        let ctx = TestContext::new();

        let app_state = AppState {
            pool: get_test_pool(ctx.db_name.clone()),
            log: Config::configure_log(),
        };

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(app_state))
                .service(register)
                .service(login),
        )
        .await;

        // Register User
        let data = FormRegister {
            username: "testuser".to_string(),
            email: EMAIL.to_string(),
            password: PASSWORD.to_string(),
        };

        let req = test::TestRequest::post()
            .uri("/register")
            .set_json(data)
            .to_request();

        let _: RegisterResponse = test::call_and_read_body_json(&app, req).await;

        // Login
        let req = test::TestRequest::post()
            .uri("/login")
            .set_json(form)
            .to_request();

        let resp: LoginResponse = test::call_and_read_body_json(&app, req).await;
        resp
    }

    #[actix_web::test]
    /// Returns an error message when the username contains invalid characters
    async fn test_login_success() {
        let data = FormLogin {
            email: EMAIL.to_string(),
            password: PASSWORD.to_string(),
        };

        let resp = test_login(data).await;

        assert_eq!(resp.error, None);
        assert_eq!(resp.user.unwrap().email, EMAIL);
    }

    #[actix_web::test]
    /// Returns an error message when the email is wrong
    async fn test_login_email_wrong() {
        let data = FormLogin {
            email: "asdf@ddd.dd".to_string(),
            password: PASSWORD.to_string(),
        };

        let resp = test_login(data).await;

        assert_eq!(resp.error, Some(MSG_LOGIN_WRONG_CREDENTIALS.to_string()));
        assert_eq!(resp.user, None);
        assert_eq!(resp.api_key, None);
    }

    #[actix_web::test]
    /// Returns an error message when the password is wrong
    async fn test_login_password_wrong() {
        let data = FormLogin {
            email: EMAIL.to_string(),
            password: "asdfdsaf".to_string(),
        };

        let resp = test_login(data).await;

        assert_eq!(resp.error, Some(MSG_LOGIN_WRONG_CREDENTIALS.to_string()));
        assert_eq!(resp.user, None);
        assert_eq!(resp.api_key, None);
    }
}
