use crate::helpers::errors::{AppError, log_error};
use crate::models::app_state::AppState;
use crate::helpers::session_state::TypedSession;

use actix_web::{post, web, Responder, Result};
use serde::{Deserialize, Serialize};
use slog::o;

#[derive(Serialize, Deserialize)]
pub struct LogoutResponse {
    success: bool,
    error: Option<String>,
}

#[post("/logout")]
async fn logout(state: web::Data<AppState>, session: TypedSession) -> Result<impl Responder, AppError> {
    let sublog = state.log.new(o!("handler" => "logout"));

    // let conn = &mut get_conn(state.pool.clone(), sublog).await?;

    match session.get_user_id() {
        Ok(_) => {
            session.purge();
            Ok(web::Json(LogoutResponse {
                success: true,
                error: None,
            }))
        }
        Err(err) => Err(log_error(sublog)(err))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::db::TestContext;
    use crate::helpers::errors::AppErrorResponse;
    use crate::tests::test_setup;
    use crate::helpers::messages::*;
    
    use actix_web::http::header;
    use actix_web::test;

    async fn test_logout_setup() -> LogoutResponse {
        let ctx = TestContext::new();

        let (cookie, app) = test_setup(&ctx).await;

        // Logout
        let req = test::TestRequest::post()
            .uri("/logout")
            .append_header((header::COOKIE, cookie.clone()))
            .set_json(())
            .to_request();

        let resp: LogoutResponse = test::call_and_read_body_json(&app, req).await;
        resp
    }

    #[actix_web::test]
    /// I can logout when logged in
    async fn test_logout() {
        let resp = test_logout_setup().await;

        assert_eq!(resp.error, None);
        assert_eq!(resp.success, true);
    }

    #[actix_web::test]
    /// I get an error when not logged in
    async fn test_logout_not_logged_in() {
        let ctx = TestContext::new();

        let (_, app) = test_setup(&ctx).await;

        // Logout
        let req = test::TestRequest::post()
            .uri("/logout")
            .set_json(())
            .to_request();

        let resp: AppErrorResponse = test::call_and_read_body_json(&app, req).await;

        assert_eq!(resp.error, MSG_NOT_LOGGED_IN.to_string());
    }
}
