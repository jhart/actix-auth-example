use actix_web::{get, Responder};

#[get("/")]
async fn index() -> impl Responder {
    "Hello, World!"
}


#[cfg(test)]
mod tests {
    use actix_web::{body, http::header::ContentType, test, App};

    use super::*;

    #[actix_web::test]
    async fn test_index_get() {
        let app = test::init_service(App::new().service(index)).await;
        let req = test::TestRequest::default()
            .insert_header(ContentType::plaintext())
            .to_request();
        let resp = test::call_service(&app, req).await;
        
        assert!(resp.status().is_success());
        
        let body = resp.into_body();
        let bytes = body::to_bytes(body).await;
        match std::str::from_utf8(&bytes.unwrap()) {
            Ok(s) => assert_eq!(s, "Hello, World!"),
            Err(_) => panic!("Error")
        };
    }

}