use crate::db::get_conn;
use crate::helpers::errors::{log_error, AppError, AppErrorType, log_error_type};
use crate::helpers::validators::{validate_password, validate_email, validate_username};
use crate::models::app_state::AppState;
use crate::models::forms::FormRegister;
use crate::models::user::User;

use actix_web::{post, web, Responder, Result};
use serde::{Deserialize, Serialize};
use slog::o;

#[derive(Serialize, Deserialize)]
pub struct RegisterResponse {
    success: bool,
    error: Option<String>,
}

#[post("/register")]
async fn register(
    form_user: web::Json<FormRegister>,
    state: web::Data<AppState>,
) -> Result<impl Responder, AppError> {
    let sublog = state.log.new(o!("handler" => "login"));

    let conn = &mut get_conn(state.pool.clone(), sublog.clone()).await?;

    let user = FormRegister {
        email: form_user.email.to_owned(),
        username: form_user.username.to_owned(),
        password: form_user.password.to_owned(),
    };

    // Test email
    if !validate_email(&user.email) {
        return Err(log_error_type(sublog)(AppErrorType::InvalidEmail))
    }

    // Test username
    if !validate_username(&user.username) {
        return Err(log_error_type(sublog)(AppErrorType::RegisterUsernameInvalid));
    }

    // Test password
    if !validate_password(&user.password) {
        return Err(log_error_type(sublog)(AppErrorType::PasswordInvalid));
    }

    // Create user
    match User::create(conn, user) {
        Ok(_res) => Ok(web::Json(RegisterResponse {
            success: true,
            error: None,
        })),
        Err(err) => Err(log_error(sublog)(err))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::{config::Config, errors::AppErrorResponse, messages::*};
    use actix_web::{test, App};
    use serde::de::DeserializeOwned;

    async fn test_register<T: DeserializeOwned>(form: FormRegister) -> T {
        use crate::db::get_test_pool;
        use crate::db::TestContext;

        let ctx = TestContext::new();

        let app_state = AppState {
            pool: get_test_pool(ctx.db_name.clone()),
            log: Config::configure_log(),
        };

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(app_state))
                .service(register),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/register")
            .set_json(form)
            .to_request();

        let resp: T = test::call_and_read_body_json(&app, req).await;
        resp
    }

    #[actix_web::test]
    /// I can register with a valid username, email and password
    async fn test_register_success() {
        let data = FormRegister {
            username: "testuser".to_string(),
            email: "test.email@test.de".to_string(),
            password: "my_pas444sword".to_string(),
        };

        let resp: RegisterResponse = test_register(data).await;

        assert_eq!(resp.error, None);
        assert_eq!(resp.success, true);
    }

    #[actix_web::test]
    /// Returns an error message when the email is invalid
    async fn test_register_email_invalid() {
        let data = FormRegister {
            username: "testuser".to_string(),
            email: "test.email@test".to_string(),
            password: "my_password".to_string(),
        };

        let resp: AppErrorResponse = test_register(data).await;

        assert_eq!(resp.error, MSG_EMAIL_INVALID.to_string());
    }

    #[actix_web::test]
    /// Returns an error message when the username contains invalid characters
    async fn test_register_username_invalid() {
        let data = FormRegister {
            username: "test user".to_string(),
            email: "test.email@test.de".to_string(),
            password: "my_password".to_string(),
        };

        let resp: AppErrorResponse = test_register(data).await;

        assert_eq!(resp.error, MSG_REGISTER_USERNAME_INVALID.to_string());
    }

    #[actix_web::test]
    /// Returns an error message when the password is invalid
    async fn test_register_password_invalid() {
        let data = FormRegister {
            username: "testuser".to_string(),
            email: "test.email@test.de".to_string(),
            password: "my password".to_string(),
        };

        let resp: AppErrorResponse = test_register(data).await;

        assert_eq!(resp.error, MSG_PASSWORD_INVALID.to_string());

        let data = FormRegister {
            username: "testuser".to_string(),
            email: "test.email@test.de".to_string(),
            password: "my".to_string(),
        };

        let resp: AppErrorResponse = test_register(data).await;

        assert_eq!(resp.error, MSG_PASSWORD_INVALID.to_string());
    }
}
