use diesel::pg::PgConnection;
use diesel::r2d2::{Pool, ConnectionManager};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use slog::{crit, o, Logger};
use diesel::r2d2::PoolError;
use r2d2::PooledConnection;

use crate::helpers::errors::AppError;
use crate::helpers::config::Config;

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("./migrations");

pub type DbPool = Pool<ConnectionManager<PgConnection>>;

pub fn get_connection_pool() -> Pool<ConnectionManager<PgConnection>> {
    let settings = Config::init();

    let manager = ConnectionManager::<PgConnection>::new(settings.database_string);

    Pool::builder()
        .test_on_check_out(true)
        .build(manager)
        .expect("Could not build connection pool")
}

/// Helper for the route handlers to log any connection errors
pub async fn get_conn(pool: DbPool, log: Logger) -> Result<PooledConnection<ConnectionManager<PgConnection>>, AppError> {
    pool.get().map_err(|err: PoolError| {
        let sublog = log.new(o!("cause" => err.to_string()));
        crit!(sublog, "Error creating client");
        AppError::from(err)
    })
}

// -------------------------------------------------------------------------------------
// FOR TESTING -------------------------------------------------------------------------
// -------------------------------------------------------------------------------------
// https://snoozetime.github.io/2019/06/16/integration-test-diesel.html

pub fn get_test_pool(db_name: String) -> Pool<ConnectionManager<PgConnection>> {
    let settings = Config::init();

    let manager = ConnectionManager::<PgConnection>::new(format!{"{}{}", settings.database_url, db_name});

    Pool::builder()
        .test_on_check_out(true)
        .build(manager)
        .expect("Could not build connection pool")
}


/// Helper for creating test databases for each test
// Keep the databse info in mind to drop them later
#[derive(Clone)]
pub struct TestContext {
    pub db_name: String,
}

impl TestContext {
    pub fn new() -> Self {
        use diesel::{Connection, RunQueryDsl};
        use rand::Rng;

        let _ = dotenv::dotenv();

        let settings = Config::init();

        // Generate random db name
        let mut rng = rand::thread_rng();
        let db_name = format!("db_{}", rng.gen::<u32>().to_string());
        
        // First, connect to postgres db to be able to create our test
        // database.
        let conn = &mut PgConnection::establish(&settings.database_string).expect("Cannot connect to postgres database.");

        // Remove db if exists
        let disconnect_users = format!(
            "SELECT pg_terminate_backend(pid)
            FROM pg_stat_activity
            WHERE datname = '{}';",
            db_name
        );

        diesel::sql_query(disconnect_users.as_str())
            .execute(conn)
            .unwrap();

        let query = diesel::sql_query(format!("DROP DATABASE IF EXISTS {}", db_name).as_str());
        query
            .execute(conn)
            .expect(&format!("Couldn't drop database {}", db_name));

        // Create a new database for the test
        let query = diesel::sql_query(format!("CREATE DATABASE {}", db_name).as_str());
        query
            .execute(conn)
            .expect(format!("Could not create database {}", db_name).as_str());
        
        let url_new = format!("{}{}", settings.database_url, db_name);
        let conn = &mut PgConnection::establish(&url_new).expect("Cannot connect to postgres database.");

        let _ = conn.revert_all_migrations(MIGRATIONS);        
        let _ = conn.has_pending_migration(MIGRATIONS).unwrap().to_string();
        let _ = conn.run_pending_migrations(MIGRATIONS);        

        
        Self {
            db_name: db_name.to_string(),
        }
    }
}

impl Drop for TestContext {
    fn drop(&mut self) {
        use diesel::{Connection, RunQueryDsl};
        
        // println!("Dropping db {}", self.db_name);

        let settings = Config::init();

        let conn = &mut PgConnection::establish(&settings.database_string).expect("Cannot connect to postgres database.");

        let disconnect_users = format!(
            "SELECT pg_terminate_backend(pid)
            FROM pg_stat_activity
            WHERE datname = '{}';",
            self.db_name
        );

        diesel::sql_query(disconnect_users.as_str())
            .execute(conn)
            .unwrap();

        let query = diesel::sql_query(format!("DROP DATABASE IF EXISTS {}", self.db_name).as_str());
        query
            .execute(conn)
            .expect(&format!("Couldn't drop database {}", self.db_name));
    }
}
