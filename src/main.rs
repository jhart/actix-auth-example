use actix_session::{storage::RedisActorSessionStore, SessionMiddleware};
use actix_web::middleware::Logger;
use actix_web::{web, App, HttpServer};
use dotenv::dotenv;
use env_logger::Env;

use crate::helpers::secrets::Secret;
use crate::helpers::config::Config;
use crate::models::app_state::AppState;

pub mod db;
pub mod helpers;
pub mod models;
pub mod routes;
pub mod schema;
pub mod tests;


// Continue building sessions: https://www.lpalmieri.com/posts/session-based-authentication-in-rust/#3-4-actix-session
// Error: https://www.youtube.com/watch?v=NHaWAWLgtAw

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    let app_state = AppState {
        pool: db::get_connection_pool(),
        log: Config::configure_log(),
    };

    env_logger::init_from_env(Env::default().default_filter_or("info"));

    let settings = Config::init();
    let settings_2 = settings.clone();
    let secret_key = Secret::generate_key();

    let app = move || {
        App::new()
            .wrap(Logger::default())
            .wrap(
                SessionMiddleware::builder(
                    RedisActorSessionStore::new(settings.redis_uri.clone()),
                    secret_key.clone(),
                )
                .cookie_name("session_id".to_string())
                .build(),
            )
            .app_data(web::Data::new(app_state.clone()))
            .service(routes::index::index)
            .service(routes::register::register)
            .service(routes::login::login)
            .service(routes::logout::logout)
    };

    println!("\n");
    println!("> Starting server at:        \x1b[93m{}:{}\x1b[0m", settings_2.app_url, settings_2.app_port);
    println!("> Connecting to postgres at: \x1b[93m{}\x1b[0m", settings_2.database_string);
    println!("> Connecting to redis at:    \x1b[93m{}\x1b[0m", settings_2.redis_uri);
    println!("\n");
    
    HttpServer::new(app)
        .bind((settings.app_url, settings.app_port))?
        .run()
        .await
}
