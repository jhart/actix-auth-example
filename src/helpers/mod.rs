use diesel::pg::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool};

pub mod errors;
pub mod messages;
pub mod regex;
pub mod secrets;
pub mod session_state;
pub mod config;
pub mod validators;

pub type DbPool = Pool<ConnectionManager<PgConnection>>;
