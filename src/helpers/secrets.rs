use actix_web::cookie::Key;
// use actix_web::body::MessageBody;
// use sha2::Sha256;
// use hmac::{Hmac, Mac};
use dotenv::dotenv;

pub struct Secret {
    pub key: Key
}

// type HmacSha256 = Hmac<Sha256>;

impl Secret {
    pub fn generate_key() -> Key {
        Key::generate()
    }
    pub fn get_hmac_secret() -> String {
        dotenv().ok();
        std::env::var("HMAC_SECRET").expect("HMAC_SECRET must be set")
    }
}