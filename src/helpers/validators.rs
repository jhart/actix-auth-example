use regex::Regex;
use passwords::analyzer;

use crate::helpers::regex::{REGEX_EMAIL, REGEX_USERNAME};

/// Checks if the password is between 7 and 15 characters long 
/// and whether it contains special characters
pub fn validate_password(password: &str) -> bool {
    let analyzed = analyzer::analyze(password);

    // Between 7 and 15 characters
    if analyzed.length() < 7 || analyzed.length() > 15 {
        return false
    }

    // Must contain special characters
    if analyzed.symbols_count() == 0 {
        return false
    }

    return true
}

/// Checks if the username only contains letters, numbers or underscores
pub fn validate_username(username: &str) -> bool {
    let re = Regex::new(REGEX_USERNAME).unwrap();
    re.is_match(username)
}

/// Checks if email is valid
pub fn validate_email(email: &str) -> bool {
    let re = Regex::new(REGEX_EMAIL).unwrap();
    re.is_match(email)
}