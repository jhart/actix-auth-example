pub const REGEX_USERNAME:&'static str = r"^[a-zA-Z0-9_]*$";
pub const REGEX_EMAIL:&'static str = r#"^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$"#;
pub const REGEX_PASSWORD:&'static str = r#"^[a-zA-Z]\w{6,14}$"#;
pub const REGEX_URL:&'static str = r#"(https?://)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&//=]*)"#;
