
pub const MSG_EMAIL_INVALID: &'static str = "Your email address is invalid!";
pub const MSG_ERROR_UNSPECIFIED: &'static str = "Sorry, something went wrong!";
pub const MSG_LOGIN_WRONG_CREDENTIALS: &'static str = "Your credentials are wrong!";
pub const MSG_NOT_LOGGED_IN: &'static str = "Please log in!";
pub const MSG_PASSWORD_WRONG: &'static str = "Your password is wrong!";
pub const MSG_PASSWORD_INVALID: &'static str = "Your password must have between 7 and 15 characters special characters!";
pub const MSG_PASSWORDS_DONT_MATCH: &'static str = "Your passwords don't match!";
pub const MSG_REGISTER_USERNAME_INVALID: &'static str = "Your username must only contain letters, numbers or underscores!";
