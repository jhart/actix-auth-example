use actix_web::{error::ResponseError, http::StatusCode, HttpResponse};
use diesel::r2d2::PoolError;
use serde::{Deserialize, Serialize};
use std::fmt;
use slog::Logger;

use crate::helpers::messages::*;

#[derive(Debug)]
pub struct AppError {
    pub message: Option<String>,
    pub cause: Option<String>,
    pub error_type: AppErrorType,
}

impl AppError {
    pub fn db_error(error: impl ToString) -> AppError {
        AppError { message: None, cause: Some(error.to_string()), error_type: AppErrorType::DbError }
    }

    pub fn message(&self) -> String {
        match &*self {
            AppError {
                message: Some(message),
                cause: _,
                error_type: _,
            } => message.clone(),
            AppError {
                message: None,
                cause: _,
                error_type: AppErrorType::LoginWrongCredentials
            } => MSG_LOGIN_WRONG_CREDENTIALS.to_string(),
            AppError {
                message: None,
                cause: _,
                error_type: AppErrorType::InvalidEmail
            } => MSG_EMAIL_INVALID.to_string(),
            AppError {
                message: None,
                cause: _,
                error_type: AppErrorType::RegisterUsernameInvalid
            } => MSG_REGISTER_USERNAME_INVALID.to_string(),
            AppError {
                message: None,
                cause: _,
                error_type: AppErrorType::NotLoggedIn
            } => MSG_NOT_LOGGED_IN.to_string(),
            AppError {
                message: None,
                cause: _,
                error_type: AppErrorType::PasswordWrong
            } => MSG_PASSWORD_WRONG.to_string(),
            AppError {
                message: None,
                cause: _,
                error_type: AppErrorType::PasswordsDontMatch
            } => MSG_PASSWORDS_DONT_MATCH.to_string(),
            AppError {
                message: None,
                cause: _,
                error_type: AppErrorType::PasswordInvalid
            } => MSG_PASSWORD_INVALID.to_string(),
            AppError {
                message: None,
                cause: _,
                error_type: AppErrorType::DbError,
            } => "The requested item was not found".to_string(),
            _ => "An unexpected error has occurred".to_string(),
        }
    }
}

impl From<PoolError> for AppError {
    fn from(err: PoolError) -> AppError {
        AppError { message: None, cause: Some(err.to_string()), error_type: AppErrorType::PoolError }
    }
}

impl From<AppErrorType> for AppError {
    fn from(err: AppErrorType) -> AppError {
        AppError { message: None, cause: None, error_type: err }
    }
}

#[derive(Serialize, Deserialize)]
pub struct AppErrorResponse {
    pub error: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum AppErrorType {
    DbError,
    InvalidEmail,
    InvalidPassword,
    LoginWrongCredentials,
    NotFoundError,
    NotLoggedIn,
    PasswordInvalid,
    PasswordsDontMatch,
    PasswordWrong,
    PoolError,
    RegisterUsernameInvalid,
    SessionInsertError,
    SessionNotFound,
    Unspecified(String),
    UserNotFound,
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{:?}", self)
    }
}

impl ResponseError for AppError {
    fn status_code(&self) -> StatusCode {
        match self.error_type {
            AppErrorType::DbError => StatusCode::INTERNAL_SERVER_ERROR,
            AppErrorType::NotFoundError => StatusCode::NOT_FOUND,
            _ => StatusCode::SERVICE_UNAVAILABLE,
        }
    }

    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code()).json(AppErrorResponse {
            error: self.message(),
        })
    }
}

pub fn log_error(log: Logger) -> impl Fn(AppError) -> AppError {
    move |err| {
        let log = log.new(slog::o!("cause" => err.cause.clone()));
        slog::warn!(log, "{}", err.message());
        err
    }
}

pub fn log_error_type(log: Logger) -> impl Fn(AppErrorType) -> AppError {
    move |err| {
        let err = AppError::from(err);
        let log = log.new(slog::o!("cause" => err.cause.clone()));
        slog::warn!(log, "{}", err.message());
        err
    }
}