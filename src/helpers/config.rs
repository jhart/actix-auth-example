use dotenv::dotenv;
use serde::Serialize;
use std::env;
use slog::{o, Drain, Logger};
use slog_async;
// use slog_envlogger;
use slog_term;

#[derive(Clone, Default, Debug, Serialize)]
pub struct Config {
    pub app_port: u16,
    pub app_url: String,
    pub database_url: String,
    pub database_name: String,
    pub database_string: String,
    pub redis_uri: String,
}

impl Config {
    pub fn init() -> Self {
        dotenv().ok();
        
        let app_url = env::var("APP_URL").unwrap_or("127.0.0.1".to_string());
        let app_port = env::var("APP_PORT").unwrap_or("8000".to_string());

        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let database = database_url.rsplit_once("/").unwrap_or_default();
        let redis_uri = env::var("REDIS_URI").expect("DATABASE_URL must be set");
        
        let settings = Config {
            app_url,
            app_port: app_port.parse::<u16>().unwrap_or(8000),
            database_url: format!("{}/", database.0.to_string()),
            database_name: database.1.to_string(),
            database_string: format!("{}/{}", database.0.to_string(), database.1.to_string() ),
            redis_uri,
        };
        
        // println!("{:?}", settings);

        settings
    }

    pub fn configure_log() -> Logger {
        let decorator = slog_term::TermDecorator::new().build();

        let console_drain = slog_term::FullFormat::new(decorator).build().fuse();
        // let console_drain = slog_envlogger::new(console_drain);
        let console_drain = slog_async::Async::new(console_drain).build().fuse();
        
        slog::Logger::root(console_drain, o!("v" => env!("CARGO_PKG_VERSION")))
    }
}