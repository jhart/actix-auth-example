{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs }: {

    nixosConfigurations.container = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules =
        [
          ({ pkgs, ... }: {
            boot.isContainer = true;

            system.stateVersion = "22.11";

            # Let 'nixos-version --json' know about the Git revision
            # of this flake.
            system.configurationRevision = nixpkgs.lib.mkIf (self ? rev) self.rev;

            # Network configuration.
            networking.useDHCP = false;
            networking.firewall = {
              enable = false;
              # firewall.allowedTCPPorts = [ 80 5432 6379 ];
              # firewall.allowedUDPPorts = [ 80 5432 6379 ];
            };

            # Enable redis
            services.redis.servers = {
              cix = {
                enable = true;
                databases = 16;
                logLevel = "debug";
                port = 6379;
                bind = "0.0.0.0";
                extraParams = [
                  "--protected-mode no"
                ];
              };
            };

            # Enable postgres.
            services.postgresql = {
              enable = true;
              package = pkgs.postgresql;
              port = 5432;
              ensureUsers = [
                {
                  name = "knut";
                  ensurePermissions = {
                    "ALL TABLES IN SCHEMA public" = "ALL PRIVILEGES";
                  };
                }
              ];
              enableTCPIP = true;
              authentication = pkgs.lib.mkOverride 10 ''
                local all all trust
                host all all 127.0.0.1/32 trust
                host all all ::1/128 trust
                host  all  all 0.0.0.0/0 md5
              '';

              initialScript = pkgs.writeText "initScript" ''
                CREATE ROLE knut WITH LOGIN PASSWORD 'knut' SUPERUSER CREATEDB CREATEROLE REPLICATION BYPASSRLS;
                CREATE DATABASE cix;
                GRANT ALL PRIVILEGES ON DATABASE cix TO knut;
                ALTER USER postgres PASSWORD 'postgres';
              '';
            };
          })
        ];
    };

  };
}

# sudo nixos-container create cix --flake flake.nix
# sudo nixos-container start cix
# curl http://cix
