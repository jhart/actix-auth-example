# What is it?
This example project contains a basic rust server with routes for authentication and basic account management. 

Being a basic example or starting point, authentication is provided by the server, OAUTH and other services are not implemented. Same goes for account verification via email.

# Routes
* register
* login
* logout
* change password

# Stack
* [Redis](https://redis.com/) for storing session data
* [Postgres](https://www.postgresql.org/) for storing data
* [actix_web](https://actix.rs/), an "extremely fast web framework"
* [actix_session](https://docs.rs/actix-session/latest/actix_session/) for user session management
* [Diesel](https://diesel.rs/) ORM with connection pooling
* [Slog](https://docs.rs/slog/latest/slog/) for server logs

# Features
* Fully tested with each tests creating its own database
* Custom errors
* Customizeable error messages
* email, username and password validations
* use environment variables for deployment

# Setup
    clone https://gitlab.com/jhart/actix-auth-server .
    cd actix-auth-server

You need to set environment variables for the server and key generation
    APP_PORT=8080
    APP_URL=127.0.0.1
    DATABASE_URL=postgresql://...
    HMAC_SECRET=""
    REDIS_URI="..."

# Run
    cargo run


# Run Tests
    cargo test